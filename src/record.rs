use axum::{
    body::BoxBody,
    response::{Headers, IntoResponse},
};
use http::StatusCode;
use serde::{Deserialize, Serialize};

#[derive(Debug, Eq, PartialEq, Clone, Deserialize, Serialize)]
pub struct Record {
    pub value: String,
    pub seq_no: u64,
}

#[derive(Default)]
pub(crate) struct RecordResponse {
    status_code: StatusCode,
    headers: Vec<(&'static str, String)>,
    value: String,
}

impl RecordResponse {
    pub fn new(status_code: StatusCode) -> Self {
        Self {
            status_code,
            ..Self::default()
        }
    }
    pub fn with_status(mut self, status_code: StatusCode) -> Self {
        self.status_code = status_code;
        self
    }
    pub fn with_seq_no(self, seq_no: u64) -> Self {
        self.with_header("ETag", seq_no)
    }
    pub fn with_header(mut self, tag: &'static str, value: impl ToString) -> Self {
        self.headers.push((tag, value.to_string()));
        self
    }
    pub fn conflict(record: Record) -> Self {
        Self::from(record).with_status(StatusCode::CONFLICT)
    }
}

impl IntoResponse for RecordResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        (self.status_code, Headers(self.headers), self.value).into_response()
    }
}

impl From<Record> for RecordResponse {
    fn from(record: Record) -> Self {
        Self {
            value: record.value,
            ..Default::default()
        }
        .with_seq_no(record.seq_no)
    }
}
