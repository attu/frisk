use axum::{
    async_trait,
    body::BoxBody,
    extract::{FromRequest, RequestParts},
    response::IntoResponse,
};
use http::StatusCode;
use serde::Serialize;

use crate::record::RecordResponse;

#[derive(Debug, Serialize)]
pub(crate) struct SeqNo(pub u64);

#[async_trait]
impl<B> FromRequest<B> for SeqNo
where
    B: Send,
{
    type Rejection = (StatusCode, &'static str);

    async fn from_request(req: &mut RequestParts<B>) -> Result<Self, Self::Rejection> {
        req.headers()
            .ok_or((StatusCode::BAD_REQUEST, "ETag header is missing"))?
            .get(http::header::ETAG)
            .and_then(|v| v.to_str().ok())
            .map(str::parse::<u64>)
            .ok_or((StatusCode::BAD_REQUEST, "ETag header is missing"))?
            .map(SeqNo)
            .map_err(|_| (StatusCode::BAD_REQUEST, "Failed to read SeqNo from ETag"))
    }
}

impl IntoResponse for SeqNo {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        RecordResponse::default()
            .with_seq_no(self.0)
            .into_response()
    }
}
