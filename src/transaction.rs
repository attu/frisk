use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{record::Record, seq_no::SeqNo};

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub(crate) enum Action {
    Create { value: String },
    Read,
    Updated { value: String, seq_no: u64 },
    Delete { seq_no: u64 },
    ForceUpdate { value: String },
    ForceDelete,
}

#[derive(PartialEq, Eq)]
pub(crate) enum ActionType {
    Create,
    Read,
    Updated,
    Delete,
    ForceUpdate,
    ForceDelete,
}

impl From<&'_ Action> for ActionType {
    fn from(action: &'_ Action) -> Self {
        match action {
            Action::Create { .. } => Self::Create,
            Action::Read => Self::Read,
            Action::Updated { .. } => Self::Updated,
            Action::Delete { .. } => Self::Delete,
            Action::ForceUpdate { .. } => Self::ForceUpdate,
            Action::ForceDelete => Self::ForceDelete,
        }
    }
}

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
pub(crate) enum CompletedAction {
    Create { seq_no: SeqNo },
    Read { record: Option<Record> },
    Update { seq_no: SeqNo, record: Record },
    Delete { record: Record },
    ForceUpdate { seq_no: SeqNo },
    ForceDelete { record: Option<Record> },
}

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
pub(crate) enum TransactionResponse {
    Completed(HashMap<String, CompletedAction>),
    Failed(HashMap<String, Option<Record>>),
}
