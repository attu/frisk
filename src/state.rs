use std::{
    collections::{hash_map::Entry, HashMap, HashSet},
    sync::Arc,
};

use axum::{body::BoxBody, response::IntoResponse};
use http::StatusCode;
use itertools::any;
use serde::{Deserialize, Serialize};
use tokio::sync::RwLock;

use crate::{
    record::{Record, RecordResponse},
    seq_no::SeqNo,
    transaction::{Action, CompletedAction, TransactionResponse},
};

#[derive(Default, Serialize, Deserialize, Clone)]
pub(crate) struct State {
    db: HashMap<String, Record>,
    seq_no: u64,
}

pub(crate) enum CreateResponse {
    Conflict(Record),
    Created(SeqNo),
}

pub(crate) enum ReadResponse {
    Found(Record),
    NotFound,
}

pub(crate) enum UpdateResponse {
    Conflict(Record),
    Updated(SeqNo),
    NotFound,
}

pub(crate) enum DeleteResponse {
    Conflict(Record),
    Deleted(Record),
    NotFound,
}

pub(crate) struct ForceUpdateResponse {
    seq_no: SeqNo,
    record: Option<Record>,
}

pub(crate) enum ForceDeleteResponse {
    Deleted(Record),
    NotFound,
}

impl State {
    pub(crate) fn create(&mut self, key: String, value: String) -> CreateResponse {
        match self.db.entry(key) {
            Entry::Occupied(entry) => CreateResponse::Conflict(entry.get().clone()),
            Entry::Vacant(entry) => {
                self.seq_no += 1;
                entry.insert(Record {
                    value,
                    seq_no: self.seq_no,
                });
                CreateResponse::Created(SeqNo(self.seq_no))
            }
        }
    }

    pub(crate) fn read(&self, key: &str) -> ReadResponse {
        self.db
            .get(key)
            .cloned()
            .map(ReadResponse::Found)
            .unwrap_or(ReadResponse::NotFound)
    }

    pub(crate) fn update(&mut self, key: String, value: String, seq_no: u64) -> UpdateResponse {
        if let Entry::Occupied(mut entry) = self.db.entry(key) {
            if entry.get().seq_no == seq_no {
                self.seq_no += 1;
                entry.insert(Record {
                    value,
                    seq_no: self.seq_no,
                });
                UpdateResponse::Updated(SeqNo(self.seq_no))
            } else {
                UpdateResponse::Conflict(entry.get().clone())
            }
        } else {
            UpdateResponse::NotFound
        }
    }

    pub(crate) fn delete(&mut self, key: String, seq_no: u64) -> DeleteResponse {
        if let Entry::Occupied(record) = self.db.entry(key) {
            if record.get().seq_no == seq_no {
                DeleteResponse::Deleted(record.remove())
            } else {
                DeleteResponse::Conflict(record.get().clone())
            }
        } else {
            DeleteResponse::NotFound
        }
    }

    pub(crate) fn clear(&mut self) -> impl IntoResponse {
        self.seq_no += 1;
        self.db.clear();
        StatusCode::NO_CONTENT
    }

    pub(crate) fn force_update(&mut self, key: String, value: String) -> ForceUpdateResponse {
        self.seq_no += 1;
        let record = Record {
            value,
            seq_no: self.seq_no,
        };
        let record = match self.db.entry(key) {
            Entry::Occupied(mut entry) => Some(entry.insert(record)),
            Entry::Vacant(entry) => {
                entry.insert(record);
                None
            }
        };
        ForceUpdateResponse {
            seq_no: SeqNo(self.seq_no),
            record,
        }
    }

    pub(crate) fn force_delete(&mut self, key: &str) -> ForceDeleteResponse {
        self.db
            .remove(key)
            .map(ForceDeleteResponse::Deleted)
            .unwrap_or(ForceDeleteResponse::NotFound)
    }

    pub(crate) fn list_keys(&self) -> HashSet<String> {
        self.db.keys().cloned().collect()
    }

    pub(crate) fn transaction(&mut self, actions: HashMap<String, Action>) -> TransactionResponse {
        let is_valid = !any(actions.iter(), |(key, action)| !match action {
            Action::Create { .. } => !self.db.contains_key(key),
            Action::Read => true,
            Action::Updated { seq_no, .. } => self.check_precondition(key, *seq_no),
            Action::Delete { seq_no } => self.check_precondition(key, *seq_no),
            Action::ForceUpdate { .. } => true,
            Action::ForceDelete => true,
        });
        if !is_valid {
            TransactionResponse::Failed(
                actions
                    .into_keys()
                    .map(|key| (self.db.get(key.as_str()).cloned(), key))
                    .map(|(record, key)| (key, record))
                    .collect(),
            )
        } else {
            TransactionResponse::Completed(
                actions
                    .into_iter()
                    .map(|(key, action)| {
                        let action = match action {
                            Action::Create { value } => CompletedAction::Create {
                                seq_no: self.force_update(key.clone(), value).seq_no,
                            },
                            Action::Read => CompletedAction::Read {
                                record: self.db.get(key.as_str()).cloned(),
                            },
                            Action::Updated { value, .. } => {
                                let ForceUpdateResponse { seq_no, record } =
                                    self.force_update(key.clone(), value);
                                CompletedAction::Update {
                                    seq_no,
                                    record: record.expect("internal server error"),
                                }
                            }
                            Action::Delete { .. } => {
                                let record = match self.force_delete(key.as_str()) {
                                    ForceDeleteResponse::Deleted(record) => record,
                                    ForceDeleteResponse::NotFound => {
                                        panic!("internal server error");
                                    }
                                };
                                CompletedAction::Delete { record }
                            }
                            Action::ForceUpdate { value } => {
                                let ForceUpdateResponse { seq_no, .. } =
                                    self.force_update(key.clone(), value);
                                CompletedAction::ForceUpdate { seq_no }
                            }
                            Action::ForceDelete => {
                                let record = match self.force_delete(key.as_str()) {
                                    ForceDeleteResponse::Deleted(record) => Some(record),
                                    ForceDeleteResponse::NotFound => None,
                                };
                                CompletedAction::ForceDelete { record }
                            }
                        };
                        (key, action)
                    })
                    .collect(),
            )
        }
    }

    fn check_precondition(&self, key: &str, seq_no: u64) -> bool {
        self.db
            .get(key)
            .map(|record| record.seq_no == seq_no)
            .unwrap_or(false)
    }
}

pub(crate) type SharedState = Arc<RwLock<State>>;

impl IntoResponse for CreateResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        let resp = match self {
            CreateResponse::Conflict(record) => RecordResponse::conflict(record),
            CreateResponse::Created(seq_no) => {
                RecordResponse::new(StatusCode::CREATED).with_seq_no(seq_no.0)
            }
        };
        resp.into_response()
    }
}

impl IntoResponse for ReadResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        let resp = match self {
            ReadResponse::Found(record) => RecordResponse::from(record),
            ReadResponse::NotFound => RecordResponse::new(StatusCode::NOT_FOUND),
        };
        resp.into_response()
    }
}

impl IntoResponse for UpdateResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        let resp = match self {
            UpdateResponse::Conflict(record) => RecordResponse::conflict(record),
            UpdateResponse::Updated(seq_no) => {
                RecordResponse::new(StatusCode::OK).with_seq_no(seq_no.0)
            }
            UpdateResponse::NotFound => RecordResponse::new(StatusCode::NOT_FOUND),
        };
        resp.into_response()
    }
}

impl IntoResponse for DeleteResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        let resp = match self {
            DeleteResponse::Conflict(record) => RecordResponse::conflict(record),
            DeleteResponse::Deleted(record) => RecordResponse::from(record),
            DeleteResponse::NotFound => RecordResponse::new(StatusCode::NOT_FOUND),
        };
        resp.into_response()
    }
}

impl IntoResponse for ForceUpdateResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        self.seq_no.into_response()
    }
}

impl IntoResponse for ForceDeleteResponse {
    type Body = BoxBody;

    type BodyError = axum::Error;

    fn into_response(self) -> http::Response<Self::Body> {
        let resp = match self {
            ForceDeleteResponse::Deleted(record) => RecordResponse::from(record),
            ForceDeleteResponse::NotFound => RecordResponse::new(StatusCode::NOT_FOUND),
        };
        resp.into_response()
    }
}
