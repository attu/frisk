use std::{collections::HashMap, ops::DerefMut};

use axum::{
    extract::{ContentLengthLimit, Extension, Path, Query},
    response::IntoResponse,
    routing::{get, post, put},
    AddExtensionLayer, Json, Router,
};
use serde::Deserialize;

use crate::{
    seq_no::SeqNo,
    state::{SharedState, State},
    transaction::Action,
};

pub fn app() -> Router {
    Router::new()
        .route("/version", get(version))
        .route(
            "/record/:key",
            post(create).get(read).put(update).delete(delete),
        )
        .route("/force/:key", put(force_update).delete(force_delete))
        .route("/clear", post(clear))
        .route("/keys", get(list_keys))
        .route("/transaction", post(transaction))
        .route("/state", get(dump).post(load))
        .layer(AddExtensionLayer::new(SharedState::default()))
}

const CONTENT_LENGTH_LIMIT: u64 = 5 * 1024;

async fn dump(Extension(state): Extension<SharedState>) -> impl IntoResponse {
    Json(state.read().await.clone())
}

async fn load(Json(state): Json<State>, Extension(ptr): Extension<SharedState>) {
    let mut lock = ptr.write().await;
    *lock.deref_mut() = state;
}

async fn clear(Extension(state): Extension<SharedState>) -> impl IntoResponse {
    state.write().await.clear()
}

async fn force_update(
    Path(key): Path<String>,
    ContentLengthLimit(data): ContentLengthLimit<String, { CONTENT_LENGTH_LIMIT }>,
    Extension(state): Extension<SharedState>,
) -> impl IntoResponse {
    state.write().await.force_update(key, data)
}

async fn force_delete(
    Path(key): Path<String>,
    Extension(state): Extension<SharedState>,
) -> impl IntoResponse {
    state.write().await.force_delete(&key)
}

async fn create(
    Path(key): Path<String>,
    ContentLengthLimit(data): ContentLengthLimit<String, { CONTENT_LENGTH_LIMIT }>,
    Extension(state): Extension<SharedState>,
) -> impl IntoResponse {
    state.write().await.create(key, data)
}

async fn read(
    Path(key): Path<String>,
    Extension(state): Extension<SharedState>,
) -> impl IntoResponse {
    state.read().await.read(key.as_str())
}

async fn update(
    Path(key): Path<String>,
    ContentLengthLimit(data): ContentLengthLimit<String, { CONTENT_LENGTH_LIMIT }>,
    Extension(state): Extension<SharedState>,
    SeqNo(seq_no): SeqNo,
) -> impl IntoResponse {
    state.write().await.update(key, data, seq_no)
}

async fn delete(
    Path(key): Path<String>,
    Extension(state): Extension<SharedState>,
    SeqNo(seq_no): SeqNo,
) -> impl IntoResponse {
    state.write().await.delete(key, seq_no)
}

async fn transaction(
    Json(actions): Json<HashMap<String, Action>>,
    Extension(state): Extension<SharedState>,
) -> impl IntoResponse {
    let resp = state.write().await.transaction(actions);
    Json(resp)
}

async fn version() -> impl IntoResponse {
    Json("0.1")
}

async fn list_keys(
    Extension(state): Extension<SharedState>,
    Query(_query): Query<ListKeysQuery>,
) -> impl IntoResponse {
    Json(state.read().await.list_keys())
}

fn no_seq_no() -> bool {
    false
}

#[derive(Debug, Deserialize)]
struct ListKeysQuery {
    #[serde(default = "no_seq_no")]
    with_seq_no: bool,
}
