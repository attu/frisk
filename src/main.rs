use std::{net::TcpListener, path::PathBuf};

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(short, long, default_value = "0.0.0.0")]
    host: String,
    #[structopt(short, long, default_value = "0")]
    port: u16,
    #[structopt(short, long)]
    load: Option<PathBuf>,
}

#[tokio::main]
async fn main() {
    let args = Args::from_args();
    let addr = tokio::net::lookup_host((args.host.as_str(), args.port))
        .await
        .unwrap()
        .next()
        .unwrap();
    let listener = TcpListener::bind(addr).unwrap();
    let addr = listener.local_addr().unwrap();
    println!("{:?}", addr);

    axum::Server::from_tcp(listener)
        .unwrap()
        .serve(frisk::app().into_make_service())
        .await
        .unwrap();
}
