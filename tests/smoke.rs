mod test_client;
mod test_server;

#[cfg(test)]
mod tests {

    use frisk::Record;

    use crate::{
        test_client::{Client, Create, Delete, Update},
        test_server::TestServer,
    };

    #[tokio::test]
    async fn smoke_test() {
        let srv = TestServer::default();
        let client = Client::new(srv.local_addr());

        assert!(client.read("lorem").await.unwrap().is_none());
        assert!(matches!(
            client.create("lorem", "ipsum").await.unwrap(),
            Create::Created(1)
        ));
        assert_eq!(
            client.read("lorem").await.unwrap().unwrap(),
            Record {
                value: "ipsum".into(),
                seq_no: 1
            }
        );
        assert!(matches!(
            client.create("lorem", "dolor").await.unwrap(),
            Create::Conflict(Record {
                seq_no: 1,
                value,
            }) if value == "ipsum"
        ));
        assert!(matches!(
            client.update("dolor", "sit", 1).await.unwrap(),
            Update::NotFound
        ));
        assert!(matches!(
            client.update("lorem", "dolor", 2).await.unwrap(),
            Update::Conflict(Record{seq_no: 1, value}) if value == "ipsum"
        ));
        assert!(matches!(
            client.update("lorem", "dolor", 1).await.unwrap(),
            Update::Updated(2)
        ));

        assert!(matches!(
            client.delete("lorem", 1).await.unwrap(),
            Delete::Conflict(Record { seq_no: 2, value }) if value == "dolor"
        ));
        assert!(matches!(
            client.delete("lorem", 2).await.unwrap(),
            Delete::Deleted(Record { seq_no: 2, value }) if value == "dolor"
        ));
        assert!(matches!(
            client.delete("lorem", 2).await.unwrap(),
            Delete::NotFound
        ));
    }

    #[tokio::test]
    async fn can_get_server_version() {
        let srv = TestServer::default();
        let client = Client::new(srv.local_addr());

        assert_eq!(client.version().await.unwrap(), r#""0.1""#)
    }
}
