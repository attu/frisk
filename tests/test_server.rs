use std::net::{SocketAddr, TcpListener, ToSocketAddrs};

use tokio::task::JoinHandle;

pub struct TestServer {
    local_addr: SocketAddr,
    _join_handle: JoinHandle<()>,
}

impl TestServer {
    pub fn new(addr: impl ToSocketAddrs) -> std::io::Result<Self> {
        let listener = TcpListener::bind(addr)?;
        let local_addr = listener.local_addr()?;
        let _join_handle = tokio::spawn(async move {
            axum::Server::from_tcp(listener)
                .unwrap()
                .serve(frisk::app().into_make_service())
                .await
                .unwrap();
        });
        Ok(Self {
            local_addr,
            _join_handle,
        })
    }

    pub fn local_addr(&self) -> SocketAddr {
        self.local_addr
    }
}

impl Default for TestServer {
    fn default() -> Self {
        Self::new(SocketAddr::from(([0, 0, 0, 0], 0))).unwrap()
    }
}
