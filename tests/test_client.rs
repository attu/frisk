use std::net::SocketAddr;

use anyhow::anyhow;
use http::{Request, StatusCode};
use hyper::{client::HttpConnector, Body};

use frisk::Record;

pub struct Client {
    inner: hyper::Client<HttpConnector>,
    local_addr: SocketAddr,
}

#[derive(Debug)]
pub enum Create {
    Created(u64),
    Conflict(Record),
}

#[derive(Debug)]
pub enum Update {
    Updated(u64),
    Conflict(Record),
    NotFound,
}

#[derive(Debug)]
pub enum Delete {
    Deleted(Record),
    Conflict(Record),
    NotFound,
}

fn read_seq_no(resp: &hyper::Response<hyper::Body>) -> anyhow::Result<u64> {
    Ok(resp
        .headers()
        .get("ETag")
        .ok_or_else(|| anyhow!("no ETag header"))?
        .to_str()?
        .parse::<u64>()?)
}

async fn read_value(resp: hyper::Response<hyper::Body>) -> anyhow::Result<String> {
    Ok(String::from_utf8(
        hyper::body::to_bytes(resp.into_body()).await?.to_vec(),
    )?)
}

async fn read_record(resp: hyper::Response<hyper::Body>) -> anyhow::Result<Record> {
    let seq_no = read_seq_no(&resp)?;
    let value = read_value(resp).await?;
    Ok(Record { value, seq_no })
}

impl Client {
    pub fn new(local_addr: SocketAddr) -> Self {
        Self {
            inner: hyper::Client::new(),
            local_addr,
        }
    }

    pub async fn version(&self) -> anyhow::Result<String> {
        let req = Request::builder()
            .uri(format!("http://{}/version", self.local_addr))
            .body(Body::empty())?;
        let resp = self.inner.request(req).await?;
        read_value(resp).await
    }

    pub async fn read(&self, key: &str) -> anyhow::Result<Option<Record>> {
        let req = Request::builder()
            .uri(format!("http://{}/record/{}", self.local_addr, key))
            .body(Body::empty())?;
        let resp = self.inner.request(req).await?;
        match resp.status() {
            StatusCode::NOT_FOUND => Ok(None),
            StatusCode::OK => Ok(Some(read_record(resp).await?)),
            status => Err(anyhow!(
                "server returns unexpected status code {:?}",
                status
            )),
        }
    }

    pub async fn create(&self, key: &str, value: impl ToString) -> anyhow::Result<Create> {
        let req = Request::builder()
            .uri(format!("http://{}/record/{}", self.local_addr, key))
            .method("POST")
            .body(hyper::Body::from(value.to_string()))?;
        let resp = self.inner.request(req).await?;
        match resp.status() {
            StatusCode::CREATED => Ok(Create::Created(read_seq_no(&resp)?)),
            StatusCode::CONFLICT => Ok(Create::Conflict(read_record(resp).await?)),
            status => Err(anyhow!(
                "server returns unexpected status code {:?}",
                status
            )),
        }
    }

    pub async fn update(
        &self,
        key: &str,
        value: impl ToString,
        seq_no: u64,
    ) -> anyhow::Result<Update> {
        let req = Request::builder()
            .uri(format!("http://{}/record/{}", self.local_addr, key))
            .method("PUT")
            .header("ETag", seq_no)
            .body(hyper::Body::from(value.to_string()))?;
        let resp = self.inner.request(req).await?;
        match resp.status() {
            StatusCode::OK => Ok(Update::Updated(read_seq_no(&resp)?)),
            StatusCode::CONFLICT => Ok(Update::Conflict(read_record(resp).await?)),
            StatusCode::NOT_FOUND => Ok(Update::NotFound),
            status => Err(anyhow!(
                "server returns unexpected status code {:?}",
                status
            )),
        }
    }

    pub async fn delete(&self, key: &str, seq_no: u64) -> anyhow::Result<Delete> {
        let req = Request::builder()
            .uri(format!("http://{}/record/{}", self.local_addr, key))
            .method("DELETE")
            .header("ETag", seq_no)
            .body(hyper::Body::empty())?;
        let resp = self.inner.request(req).await?;
        match resp.status() {
            StatusCode::OK => Ok(Delete::Deleted(read_record(resp).await?)),
            StatusCode::CONFLICT => Ok(Delete::Conflict(read_record(resp).await?)),
            StatusCode::NOT_FOUND => Ok(Delete::NotFound),
            status => Err(anyhow!(
                "server returns unexpected status code {:?}",
                status
            )),
        }
    }
}
