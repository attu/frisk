FROM rust:1.56-alpine

RUN apk add --no-cache musl-dev

COPY src /src
COPY Cargo.toml Cargo.toml
COPY Cargo.lock Cargo.lock

RUN cargo build --release --target=x86_64-unknown-linux-musl \
 && strip /target/x86_64-unknown-linux-musl/release/frisk

FROM alpine:3.14

RUN apk add dumb-init

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

COPY --from=0 /target/x86_64-unknown-linux-musl/release/frisk /usr/bin/frisk

EXPOSE 11411

CMD ["/usr/bin/frisk", "--port", "11411"]

